package nl.utwente.di.celsiusToFahrenheitConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test the Quoter */
public class TestConverter {

    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();
        double fahrenheit = converter.convertCelsiusToFahrenheit(10.0);
        Assertions.assertEquals(50.0, fahrenheit, 0.0, "The temperature in fahrenheit is ");
    }
}
