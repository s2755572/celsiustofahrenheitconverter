package nl.utwente.di.celsiusToFahrenheitConverter;


public class Converter {

    public double convertCelsiusToFahrenheit(Double celsius) {
        return celsius * 1.8 + 32;
    }
}
